<?php

namespace App\Http\Controllers;

use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Manager;

class ApiController extends Controller
{
    // error codes
    const ERROR_INPUT           = 'ERROR VALIDATION FAILED';
    const ERROR_NOT_FOUND       = 'ERROR ITEM NOT FOUND';
    const ERROR                 = 'ERROR';

    // success codes
    const SUCCESS_DELETE        = 'ITEM DELETED';

    // message types
    const MESSAGE_ERROR         = 'error';
    const MESSAGE_NOTIFICATION  = 'message';

    protected $statusCode       = 200;

    /**
     * @param Manager $fractal
     */
    public function __construct(Manager $fractal)
    {
        $this->fractal = $fractal;
    }

    /**
     * @param $statusCode int http status code
     * @return $this
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * @param $item Object single item to be returned
     * @param $callback Transformer transformer callback to present the item
     * @return mixed
     */
    protected function respondWithItem($item, $callback)
    {
        $resource = new Item($item, $callback);
        $rootScope = $this->fractal->createData($resource);
        return $this->respondWithArray($rootScope->toArray());
    }

    /**
     * @param $collection Collection collection of items to be returned
     * @param $callback Transformer transformer callback to present each of the item
     * @return mixed
     */
    protected function respondWithCollection($collection, $callback)
    {
        $resource = new Collection($collection, $callback);
        $rootScope = $this->fractal->createData($resource);
        return $this->respondWithArray($rootScope->toArray());
    }

    /**
     * @param array $array data to be returned in the response
     * @param array $headers additional headers
     * @return mixed
     */
    protected function respondWithArray(array $array, array $headers = [])
    {
        return response()->json($array, $this->statusCode)->withHeaders($headers);
    }

    /**
     * @param $message string message to be returned to the client
     * @param $messageCode string internal code
     * @param $messageType string internal message type
     * @return mixed
     */
    protected function respondWithMessage($message, $messageCode, $messageType)
    {

        return $this->respondWithArray([
                                           $messageType => [
                                               'code' => $messageCode,
                                               'http_code' => $this->statusCode,
                                               'message' => $message,
                                           ]
                                       ]);
    }

    /**
     * @param string $message
     * @return mixed
     */
    public function errorInternalError($message = 'Internal Error')
    {
        return $this->setStatusCode(500)->respondWithMessage($message, self::ERROR, self::MESSAGE_ERROR);
    }

    /**
     * @param string $message
     * @return mixed
     */
    public function errorNotFound($message = 'Resource Not Found')
    {
        return $this->setStatusCode(404)->respondWithMessage($message, self::ERROR_NOT_FOUND, self::MESSAGE_ERROR);
    }

    /**
     * @param string $message
     * @return mixed
     */
    public function errorBadArgs($message = 'Bad Arguments')
    {
        return $this->setStatusCode(400)->respondWithMessage($message, self::ERROR_INPUT, self::MESSAGE_ERROR);
    }

    /**
     * @param string $message
     * @return mixed
     */
    public function actionSuccess($message = 'Action Completed')
    {
        return $this->setStatusCode(200)->respondWithMessage($message, self::SUCCESS_DELETE, self::MESSAGE_NOTIFICATION);
    }
}