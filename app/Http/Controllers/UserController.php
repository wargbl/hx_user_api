<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\UserTransformer;
use App\User;

class UserController extends ApiController
{
    protected $validationRules = [
        'forename' => 'required|max:255',
        'surname' => 'required|max:255',
        'email' => 'required|unique:users|email',
    ];

    /**
     * Display a paginated list of users
     *
     * @param Request $request
     * @return response
     */
    public function listing(Request $request)
    {
        $users = User::paginate();
        return $this->respondWithCollection($users, new UserTransformer);
    }

    /**
     * Create a new user
     *
     * @param Request $request
     * @return response
     */
    public function create(Request $request)
    {
        $data = $request->json()->get('user', []);

        $validator = Validator::make($data, $this->validationRules);

        if ($validator->fails()) {
            return $this->errorBadArgs('Cannot create user');
        }

        $user = new User;
        $user->forename = $data['forename'];
        $user->surname = $data['surname'];
        $user->email = $data['email'];
        $user->generateId();
        $user->save();

        return $this->respondWithItem($user, new UserTransformer);
    }

    /**
     * Display an individual user
     *
     * @param $id
     * @return response
     */
    public function show($id)
    {
        $user = User::where("user_id",$id)->first();

        if( !$user ) {
            return $this->errorNotFound('User not found');
        }

        return $this->respondWithItem($user, new UserTransformer);
    }

    /**
     * Update and existing user
     *
     * @param Request $request
     * @param $id
     * @return response
     */
    public function update(Request $request, $id)
    {
        $data = $request->json()->get('data', []);

        $validator = Validator::make($data, $this->validationRules);

        if ($validator->fails()) {
            return $this->errorBadArgs('Cannot update user');
        }

        $user = User::where("user_id",$id)->first();

        if( !$user ) {
            return $this->errorNotFound('User not found');
        }

        $user->forename = $data['forename'];
        $user->surname = $data['surname'];
        $user->email = $data['email'];
        $user->save();

        return $user;
    }

    /**
     * Delete a user
     *
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $user = User::where("user_id",$id)->first();
        if( !$user ) {
            return $this->errorNotFound('User not found');
        }

        if ( $user->delete() ) {
            return $this->actionSuccess('User deleted');
        }

        return $this->errorInternalError('Failed to delete user');
    }
}
