<?php

$app->get('/users', ['uses' => 'UserController@listing']);
$app->post('/users', ['uses' => 'UserController@create']);
$app->get('/users/{id}', ['uses' => 'UserController@show']);
$app->put('/users/{id}', ['uses' => 'UserController@update']);
$app->delete('/users/{id}', ['uses' => 'UserController@delete']);
