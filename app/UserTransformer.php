<?php

namespace App;

use League\Fractal\TransformerAbstract;
use App\User;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        return [
            'id'        => $user->user_id,
            'forename'  => $user->forename,
            'surname'   => $user->surname,
            'email'     => $user->email,
        ];
    }
}