# User API #
Simple User API built on the Lumen framework.

## Setup ##
* Composer install dependencies
* You will probably need to update the Homestead.yaml to match your environment

```
#!yaml

folders:
    - map: "/Users/james/dev/projects/hx_user_api"
      to: "/home/vagrant/hx-user-api"

sites:
    - map: homestead.app
      to: "/home/vagrant/hx-user-api/public"
```

* vagrant up - should give you fully working Homestead vagrant box
* You will need to add an entry for the app in your hosts file

```
#!bash

192.168.10.10  homestead.app
```
* At this point you should be able to navigate to homestead.app in your browser
* To actually get things working you will need to ssh into the Vagrant box and run the migrations...

```
#!bash

php artisan migrate
```
* ...and seed the database with some data
```
#!bash

php artisan db:seed
```


## API endpoints ##

* GET /users
Paginated list of users e.g ?page=2
* POST /users
Create a new user

```
#!json
{
  "data": {
    "forename": "Joe",
    "surname": "King",
    "email":  "joe@home.com"
  }
}
```

* GET /users/{id}
Retrieve a single user
* PUT /users/{id}
Update an existing user
* DELETE /users/{id}
Delete a user