<?php

$factory->define(App\User::class, function ($faker) {
    return [
        'user_id' => $faker->md5,
        'email' => $faker->safeEmail,
        'forename' => $faker->firstName,
        'surname' => $faker->lastName,
    ];
});
